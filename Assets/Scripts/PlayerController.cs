﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    private const string MESSAGE_CANVAS_NAME = "MainCanvas";
    private const string MESSAGE_TEXT_NAME = "Text";
    //private const string LASER_GAMEOBJECT_NAME = "Laser";
    private const string DATA_TEXT= 
        "Accel:\n" +
        "x: {0}\n" +
        "y: {1}\n" +
        "z: {2}\n" +
        "Gyro\n" +
        "x: {3}\n" +
        "y: {4}\n" +
        "z: {5}";
    private float secondsCount = 0.0f;
    private String timerText;
    private int minuteCount = 0;
    private int hourCount = 0;
    private IList<float> accel_values = new List<float>();

    public GameObject messageCanvas;
    public Text messageText;

    // Use this for initialization
    void Start () {
        if (messageCanvas == null)
        {
            messageCanvas = transform.Find(MESSAGE_CANVAS_NAME).gameObject;
            if (messageCanvas != null)
            {
                messageText = messageCanvas.transform.Find(MESSAGE_TEXT_NAME).GetComponent<Text>();
            }
        } else
        {
            // throw error

        }
    }

    // Update is called once per frame
    void Update()
    {
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);

        Vector3 accel_vector = GvrControllerInput.Accel;
        Vector3 gyro_vector = GvrControllerInput.Gyro;
        UpdateDataText(accel_vector, gyro_vector);

        UpdateTimerUI();

        CalculateSpeed(accel_vector, gyro_vector);

    }

    private void CalculateSpeed(Vector3 ac, Vector3 gv)
    {
        if (accel_values.Count >200)
        {
            accel_values.RemoveAt(0);
        }
        accel_values.Add(ac.x);
        messageText.text += "\nList size: " + accel_values.Count;

        int pos = 0;
        int neg = 0;
        string text = "forward";
        foreach (float val in accel_values)
        {
            if (val > 0)
            {
                pos++;
            }else
            {
                neg++;
            }
        }
        if ( pos == neg )
        {
            text = "standing";
        } else if (pos > neg)
        {
            text = "forward";
        } else
        {
            text = "back";
        }

        messageText.text += "\nMovement status: " + text; 

    }

    private void UpdateDataText(Vector3 av, Vector3 gv)
    {
        string accel_x = av.x.ToString("F2");
        string accel_y = av.y.ToString("F2");
        string accel_z = av.z.ToString("F2");
        string gyro_x = gv.x.ToString("F2");
        string gyro_y = gv.y.ToString("F2");
        string gyro_z = gv.z.ToString("F2");

        //double total_accel = accel_vector.x + accel_vector.y + accel_vector.z - 9.8;
        string formatted_text = String.Format(DATA_TEXT, accel_x, accel_y, accel_z, gyro_x, gyro_y, gyro_z);
        messageText.text = formatted_text;
        //messageCanvas.SetActive(true);
    }

    public void UpdateTimerUI()
    {
        //set timer UI
        secondsCount += Time.deltaTime;
        timerText = hourCount + "h:" + minuteCount + "m:" + (int)secondsCount + "s";
        if (secondsCount >= 60)
        {
            minuteCount++;
            secondsCount = 0;
        }
        else if (minuteCount >= 60)
        {
            hourCount++;
            minuteCount = 0;
        }

        messageText.text += "\n" + timerText;
    }

}
